#creating a function for node failure finding which wil return the node which has maximum no of inbound and outbound links
def node_failure(input_list):
  set1 = set(input_list)
  dict1={}
  max = 1

  #counting frequency of each node
  for i in input_list:
    if i in dict1:
      dict1[i] = dict1[i]+1
    else:
      dict1[i] = 1

  #counting maximum frequency
  for k in dict1.keys():
    if dict1[k] > max:
      max = dict1[k]

  #create a final list which will store the node which has maximum frequency of links
  final_list = [s for s in dict1.keys() if dict1[s] == max]
  
  #create dictionaries to store inbound, outbound and total links
  inb = {}
  out = {}
  output = {}

  for p in final_list:
    #initializing inbound and outbound with maximum
    inbound = max
    outbound = max
    if input_list[0] == p:
      inbound = max-1
    if input_list[-1] == p:
      outbound = max -1
    inb[p] = inbound
    out[p] = outbound
    output[p] = int(inb[p]) + int(out[p])

  #creating final output list which contains maximum links
  maxi = 2
  output_list = [k for k, v in output.items() if v > maxi]

  #creating new list which contains out output which will print     
  final_output = []
  for y in output_list:
    final_output.append("router "+ y + " has " + str(inb[y]) + " inbound links and " + str(out[y]) + " outbound links ")

  #printing output as per requirements
  if output_list:
    print(*output_list,sep = ',',end = ' ')
    print("* Since",end=' ')
    print(*final_output,sep='and ')
  else:
    print("No repeated nodes found on the given network structure")

if __name__ == "__main__":
  """
  sample input will be string representing the network connections
  
  example: "1->2->3->4->2->5->6->1"
  """
  raw_input_list = list(input("Enter Path:").replace("->",""))

  #creating final list which removes extra space
  input_list = [z for z in raw_input_list if z.isnumeric()==True]

  #calling function node_failure
  node_failure(input_list)
