from datetime import timedelta
from django.shortcuts import render
from django.views.generic import TemplateView, DetailView
from django.contrib.auth.views import LoginView
from django.utils import timezone
from django.contrib.auth import authenticate
from src.apps.account.models import UserDetail
from django.shortcuts import redirect
from django.utils.timezone import localtime


# function to get userdetail from UserDetail table
def get_user_details(uname):
    user = UserDetail.objects.get(username=uname)
    # check condion to know about trial status
    if (user.createdAt + timedelta(days=7)) > timezone.now():
        expired = "Running"
    else:
        expired = "Ended"
    return {"user": user, "expired": expired, "sub_expired": user.subscriptionEndAt}


# profile view for user 's profile
# It will render at successfull registration and login
class ProfileView(TemplateView, DetailView):
    modal = UserDetail
    # view's renderer function for get request

    def get(self, request, *args, **kwargs):
        user = request.user
        details = get_user_details(user)
        return render(request, 'templates/profile.html', details)


# custom lgin view to over ride django registration's LoginView
class CustomLoginView(LoginView):

    def post(self, request, *args, **kwargs):
        password = request.POST.get("password")
        username = request.POST.get("username")
        # authenticate user using password and username
        user = authenticate(username=username, password=password)
        if user:
            return redirect("/accounts/profile/")
        else:
            return render(request, "templates/error.html")
