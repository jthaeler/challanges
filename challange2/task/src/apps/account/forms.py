from django_registration.forms import RegistrationForm
from .models import UserDetail

# custom form for user register model
class CustomUserForm(RegistrationForm):
    class Meta(RegistrationForm.Meta):
        model = UserDetail
        #desired fields to be taken while user registers
        fields = ["username", "email","firstName","lastName"]