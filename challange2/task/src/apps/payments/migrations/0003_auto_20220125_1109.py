# Generated by Django 2.2.26 on 2022-01-25 05:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0002_paymentdetails_session_id'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymentdetails',
            name='cancelled',
            field=models.BooleanField(blank=True, default=False, null=True),
        ),
        migrations.AddField(
            model_name='paymentdetails',
            name='successed',
            field=models.BooleanField(blank=True, default=False, null=True),
        ),
    ]
