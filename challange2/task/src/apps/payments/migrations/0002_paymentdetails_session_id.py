# Generated by Django 2.2.26 on 2022-01-25 05:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('payments', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='paymentdetails',
            name='session_id',
            field=models.TextField(default='GdfgsdfgsdfGS'),
            preserve_default=False,
        ),
    ]
