from django.urls import path
from . import views

#urls pattern for intregration of stripe
urlpatterns = [
    path('', views.StripePurchasePage.as_view()), # initial page render /stripe/
    path('config/', views.stripe_config), #config for payment page /stripe/config/
    path('cancel_sub/', views.cancel_sub, name = "cancel_sub"), #url for cancelling subscription /stripe/cancel_sub/
    path('checkout_session/', views.create_checkout_session),
    path('success', views.SuccessView.as_view()), #url to redirect after successful stripe payment
    path('cancelled/', views.CancelledView.as_view()), #url to redirect after cancelled stripe payment
    path('cancellation_confirmed/', views.CancellationConfirmed.as_view()), #url to redirect after cancelling subscription
]
